#!/usr/bin/env python3
from lab5ex1 import ComplexNumber

if __name__ == '__main__':
    real = int(input("Enter real part of first number: "))
    imag = int(input("Enter imaginary part of first number: "))
    x = ComplexNumber(real, imag)
    real = int(input("Enter real part of second number: "))
    imag = int(input("Enter imaginary part of second number: "))
    y = ComplexNumber(real, imag)

    oper = input('Enter operation:\n1.Add\n2.Subtract\n3.Multiply\n')
    try:
        oper = int(oper)
    except ValueError:
        print('Invalid operation.')
        exit(1)

    if oper == 1:
        print('Result: {}'.format(x+y))
    elif oper == 2:
        print('Result: {}'.format(x-y))
    elif oper == 3:
        print('Result: {}'.format(x*y))
    else:
        print('Invalid operation.')
