#!/usr/bin/env python3
from xml.dom import minidom
from io import StringIO

def main():
    dom = minidom.parse('breakfast_menu.xml')

    elem = dom.getElementsByTagName('price')

    for e in elem:
        e.firstChild.nodeValue = '$9.99'
    
    with open('breakfast_menu_new.xml', 'w') as outfile:
        dom.writexml(outfile)

if __name__ == '__main__':
    main()
