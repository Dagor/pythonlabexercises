#!/usr/bin/env python3
import random

def random_matrix(size):
    mat = []
    for i in range(size):
        vec = [random.randint(0,100) for _ in range(size)]
        mat.append(vec)

    return mat

if __name__ == '__main__':
    mat1 = random_matrix(128)
    mat2 = random_matrix(128)

    mat_sum = [[None] * 128] * 128

    for i in range(len(mat1)):
        for j in range(len(mat1[i])):
            mat_sum[i][j] = mat1[i][j] + mat2[i][j]

    print(mat_sum)
            
        
