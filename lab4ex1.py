#!/usr/bin/env python3
import math

if __name__ == '__main__':
    a = int(input('Enter A coefficient: '))
    b = int(input('Enter B coefficient: '))
    c = int(input('Enter C coefficient: '))

    discriminant = b ** 2 - 4 * a * c
    if discriminant == 0:
        x1 = -b / (2 * a)
        print('Only one real root: {}'.format(x1))
    elif discriminant > 0:
        x1 = (-b + math.sqrt(discriminant)) / (2 * a)
        x2 = (-b - math.sqrt(discriminant)) / (2 * a)
        print('Two real roots: x1 = {} and x2 = {}'.format(x1, x2))
    else:
        print('No real roots.')
