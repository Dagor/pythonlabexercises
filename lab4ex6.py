#!/usr/bin/env python3
import random
import numpy.linalg

if __name__ == '__main__':
    size = random.randint(5, 100)
    mat = [[random.randint(0, 1000) for i in range(size)] for j in range(size)]
    det = numpy.linalg.det(mat)
    print('Calculated determinant: {}'.format(det))
    
