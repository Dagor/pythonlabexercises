#!/usr/bin/env python3
import os
import os.path as path
import sys

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please provide a directory to walk through.')
    else:
        for root, dirs, files in os.walk(sys.argv[1]):
            for name in files:
                print(path.join(root, name))
            for name in dirs:
                print(path.join(root, name))

