#!/usr/bin/env python3
import os.path

def check_code():
    with open('code.txt', 'r') as infile:
        #no restrictions on code length
        code = infile.read()
        user_code = input('Please enter code: ')
        if user_code == code:
            print('Codes match!')
        else:
            print('Codes do not match!')

def new_code():
    code = input('Please enter new code: ')
    with open('code.txt', 'w') as outfile:
        outfile.write(code)
        print('New code written.')

if __name__ == '__main__':
    if os.path.exists('code.txt'):
        check_code()
    else:
        new_code()
