#!/usr/bin/env python3
STR_TO_DEL = ['się', 'i', 'oraz', 'nigdy', 'dlaczego']

if __name__ == '__main__':
    user_input = input('Enter string to clean:')
    for word in STR_TO_DEL:
        user_input = user_input.replace(word, '')

    print('Output string:\n' + user_input)

    

