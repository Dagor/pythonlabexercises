#!/usr/bin/env python3
import random

def bubble_sort(arr):
    n = len(arr)
    for i in range(n):

        for j in range(0, n-i-1):
            if arr[j] > arr[j+1]:
                tmp = arr[j]
                arr[j] = arr[j+1]
                arr[j+1] = tmp

if __name__ == '__main__':
    arr = [random.randint(0, 1000) for _ in range(50)]
    new_arr = arr.copy()
    new_arr.sort()
    bubble_sort(arr)
    error = False

    for i in range(len(arr)):
        if arr[i] != new_arr[i]:
            error = True

    if error == True:
        print('Built-in and implemented functions return different values.')
    else:
        print(arr)
        print('Array sorted successfully.')
    

