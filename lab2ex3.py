#!/usr/bin/env python3
import os
import os.path
import sys

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('Please provide directory path.')
    else:
        for root, dirs, files in os.walk(sys.argv[1]):
            for f in files:
                fname = os.path.join(root, f)
                basename, ext = os.path.splitext(fname)
                if ext == '.jpg':
                    os.rename(fname, basename + '.png')

