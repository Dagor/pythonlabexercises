#!/usr/bin/env python3
import os.path
import json
from datetime import datetime

def modify_entry(data):
    item_id = input('Enter item identification number: ')

    current_item = None
    for i in range(len(data['food_items'])):
        if data['food_items'][i]['id'] == item_id:
            current_item = data['food_items'][i]
            break

    if current_item is not None:
        operation = int(input('What do you want to modify:\n1.Name\n2.Price\n3.Description\n'))
        if operation == 1:
            name = input('Please enter new name: ')
            current_item['name'] = name
        elif operation == 2:
            price = float(input('Please enter new price: '))
            current_item['price'] = price
        elif operation == 3:
            desc = input('Please enter new description: ')
            current_item['desc'] = desc
        else:
            print('Unknown operation. No modification performed.')
    else:
        print('No item with given id found.')

def remove_entry(data):
    item_id = input('Enter item identification number: ')

    for i in range(len(data['food_items'])):
        if data['food_items'][i]['id'] == item_id:
            del data['food_items'][i]
            print('Item removed.')
            return

    print('No item with given id found.')
    
def add_entry(data):
    identificator = datetime.now().strftime("%d%m%Y%H%M%S")
    name = input('Please enter food item name: ')
    price = float(input('Please enter food item price: '))
    description = input('Please enter item description: ')
    new_item = { "id" : identificator, "name" : name, "price" : price, "desc" : description }
    data['food_items'].append(new_item)

def print_data(data):
    if len(data) == 0:
        print('No entries found.')
    else:
        for i in data['food_items']:
            print(i)

def main():
    if os.path.exists('food.json'):
        with open('food.json', 'r') as infile:
            data = json.load(infile)
    else:
        data = { 'food_items' : [] }

    while True:
        operation = int(input('Which operation do you want to perform:\n'
            + '1.Print\n2.Add\n3.Remove\n4.Modify\n5.Exit\n'))

        if operation == 1:
            print_data(data)
        elif operation == 2:
            add_entry(data)
        elif operation == 3:
            remove_entry(data)
        elif operation == 4:
            modify_entry(data)
        elif operation == 5:
            break
        else:
            print('Unknown operation.')

    with open('food.json', 'w') as outfile:
        json.dump(data, outfile)
            


if __name__ == '__main__':
    main()
