#!/usr/bin/env python3
import random

if __name__ == '__main__':
    X = [[random.randint(0, 100) for i in range(8)] for j in range(8)]
    Y = [[random.randint(0, 100) for i in range(8)] for j in range(8)]

    Z = [[0] * 8] * 8

    for i in range(len(X)):
        for j in range(len(Y[0])):
            for k in range(len(Y)):
                Z[i][j] += X[i][k] * Y[k][j]

    print('Result: {}'.format(Z))
