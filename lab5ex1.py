#!/usr/bin/env python3

class ComplexNumber(object):
    
    def __init__(self, real = None, imag = None):
        self.real = real
        self.imag = imag

    def __add__(self, other):
        return ComplexNumber(self.real + other.real, self.imag + other.imag)

    def __sub__(self, other):
        return ComplexNumber(self.real - other.real, self.imag - other.imag)

    def __mul__(self, other):
        tmp = ComplexNumber()
        tmp.real = self.real * other.real - self.imag * other.imag
        tmp.imag = self.real * other.imag + self.imag * other.real
        return tmp

    def __str__(self):
        return str(self.real) + ' + ' + str(self.imag) + 'j'

if __name__ == '__main__':
    x = ComplexNumber(1, 2)
    y = ComplexNumber(2, 1)
    print(x)
    print(x+y)
    print(x-y)
    print(x*y)
