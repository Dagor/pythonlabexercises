#!/usr/bin/env python3
STR_REPLACE = { 'nigdy' : 'prawie nigdy', 'i' : 'oraz',
        'dlaczego' : 'czemu' }

if __name__ == '__main__':
    user_string = input('Please enter string to modify:\n')
    for item in STR_REPLACE.items():
        user_string = user_string.replace(item[0], item[1])

    print(user_string)
